# Fonctionnalité n°1

Ce fichier ne sera visible que sur la branche `feature1` tant que je n'aurai pas mergé sur la branche main.

L'idée ici, c'est justement de merger sur la branche principale et donc, voir ce fichier apparaitre.

Voici ma grande contribution !